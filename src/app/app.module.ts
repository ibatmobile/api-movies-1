import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//@TODO: Add in a movies page (overwrite the about page generated in tabs template)
import { MoviesPage } from '../pages/movies/movies';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//@TODO - add the http module  - otherwise you will get error message: No provider for http 
import { HttpModule } from '@angular/http';

//@TODO Add movie model,movie service, movie component
import { Movie } from '../model/movie.model'
import { MovieService } from '../providers/movie';
import { MovieComponent } from '../components/movie/movie';

@NgModule({
  declarations: [
    MyApp,
    MoviesPage,

    HomePage,
    TabsPage,
    MovieComponent
  ],
  imports: [
    BrowserModule,
    HttpModule, //@Add the http module
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MoviesPage,
  
    HomePage,
    TabsPage,
    MovieComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
   
    MovieService, //@ Add the movie service
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
