import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Rx';
import { Movie } from '../model/movie.model';

@Injectable()
export class MovieService {
  API_ENDPOINT: string;
  API_KEY: string;
  API_URL: string;





  constructor(public http: Http) {
    this.API_ENDPOINT = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=";
    this.API_KEY = "40b1737b334731260976ee0db9de4f08";
    this.API_URL = this.API_ENDPOINT + this.API_KEY

  }

  //https://scotch.io/tutorials/angular-2-http-requests-with-observables
  getListOfMovies(): Observable<any[]> {
    let serviceURL = this.API_URL;
    
    return this.http.get(serviceURL)
      // ...and calling .json() on the response to return data
      .map((res: Response) => res.json())
      //...errors if any
      .catch((error: any) => Observable.throw('Server error')

      );
  }


}
