import { Component } from '@angular/core';

import { MovieService } from '../../providers/movie'
/**
 * Generated class for the Movie component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'movie',
  templateUrl: 'movie.html'
})
export class MovieComponent {

  text: string;
  listOfMovies: any;
  constructor(public movieService: MovieService) {
    console.log('Hello Movie Component');
    this.text = 'Hello World';
    this.listOfMovies = [];  
    this.movieService.getListOfMovies().subscribe(listOfMovies => this.listOfMovies = listOfMovies);


  }




}
